#!/bin/bash

#This variables will be used on nginx.sh config to determing the port and server name
ODOO_PORT="8069"
SERVER_NAME="server.name"

sudo yum install nginx -y

sudo cp ./odoo.conf /etc/nginx/conf.d/
sudo cp ./nginx.conf /etc/nginx/

sudo sed -i -e "s/{{ odoo_port }}/$ODOO_PORT/g" -e "s/{{ server_name }}/$SERVER_NAME/g" /etc/nginx/conf.d/odoo.conf
sudo nginx -t
sudo systemctl enable --now nginx
sudo systemctl reload nginx

sudo firewall-cmd --add-service http --permanent 
sudo firewall-cmd --reload
sudo setsebool httpd_can_network_connect 1 -P
##########################################
#Update postgresql file and restart
sudo cp ./postgresql.conf /var/lib/pgsql/9.6/data
sudo systemctl restart postgresql-9.6.service
#####################################
sudo cp ./odoo.con /etc/odoo
sudo systectl restart odoo
