#!/bin/bash
################################################################################
# Script for installing Odoo V10 on CENTOS7/RHEL
# Author: Hazem Mahmoud, Anass Ahmed
#-------------------------------------------------------------------------------
# This script will install Odoo on your Centos7 server. It can install multiple Odoo instances
# in one Ubuntu because of the different xmlrpc_ports
#-------------------------------------------------------------------------------
# Make a new file:
# sudo nano odoo-install.sh
# Place this content in it and then make the file executable:
# sudo chmod +x odoo-install.sh
# Execute the script to install Odoo:
# ./odoo-install
################################################################################


#Set to true if you want to install it, false if you don't need it or have it already installed.
INSTALL_WKHTMLTOPDF="True"
#Set the default Odoo port (you still have to use -c /etc/odoo-server.conf for example to use this.)
OE_PORT="$1"
#Choose the Odoo version which you want to install. For example: 10.0, 9.0, 8.0, 7.0 or saas-6. When using 'trunk' the master version will be installed.
OE_VERSION="10.0"
# Set this to True if you want to install Odoo 10 Enterprise!
IS_ENTERPRISE="False"
#set the superadmin password (master password) 
OE_SUPERADMIN="admin"

##
###  WKHTMLTOPDF download links
## === Ubuntu Trusty x64 & x32 === (for other distributions please replace these two links,
## in order to have correct version of wkhtmltox installed, for a danger note refer to 
## https://www.odoo.com/documentation/8.0/setup/install.html#deb ):
WKHTMLTOX_X64=https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox-0.12.5-1.centos7.x86_64.rpm
WKHTMLTOX_X32=https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox-0.12.5-1.centos7.i686.rpm

#--------------------------------------------------
# Update Server
#--------------------------------------------------
echo -e "\n---- Update Server ----"
sudo yum upgrade -y
sudo yum install -y epel-release yum-presto deltarpm
sudo yum upgrade -y    

#--------------------------------------------------
# Install PostgreSQL Server
#--------------------------------------------------
echo -e "\n---- Install PostgreSQL Server ----"
sudo yum install -y https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm
sudo yum install -y postgresql96 postgresql96-server
sudo /usr/pgsql-9.6/bin/postgresql96-setup initdb
sudo systemctl enable --now postgresql-9.6

#--------------------------------------------------
# Install Dependencies
#--------------------------------------------------
echo -e "\n---- Install tool packages ----"
sudo yum install -y wget git python-pip

echo -e "\n---- Install python libraries ----"
#sudo pip install num2words
# put here your extra python packages

#--------------------------------------------------
# Install Wkhtmltopdf if needed
#--------------------------------------------------
if [ $INSTALL_WKHTMLTOPDF = "True" ]; then
  echo -e "\n---- Install wkhtml and place shortcuts on correct place for ODOO 10 ----"
  #pick up correct one from x64 & x32 versions:
  if [ "`getconf LONG_BIT`" == "64" ];then
      _url=$WKHTMLTOX_X64
  else
      _url=$WKHTMLTOX_X32
  fi
  sudo yum install -y $_url
else
  echo "Wkhtmltopdf isn't installed due to the choice of the user!"
fi

#--------------------------------------------------
# Install ODOO
#--------------------------------------------------
echo -e "\n==== Installing ODOO Server ===="
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo=https://nightly.odoo.com/10.0/nightly/rpm/odoo.repo
sudo yum install -y odoo

# Odoo Enterprise install!
if [ $IS_ENTERPRISE = "True" ]; then
    sudo su odoo -c "mkdir /var/lib/odoo/enterprise"

    GITHUB_RESPONSE=$(sudo git clone --depth 1 --branch 10.0 https://www.github.com/odoo/enterprise "/var/lib/odoo/enterprise" 2>&1)
    while [[ $GITHUB_RESPONSE == *"Authentication"* ]]; do
        echo "------------------------WARNING------------------------------"
        echo "Your authentication with Github has failed! Please try again."
        printf "In order to clone and install the Odoo enterprise version you \nneed to be an offical Odoo partner and you need access to\nhttp://github.com/odoo/enterprise.\n"
        echo "TIP: Press ctrl+c to stop this script."
        echo "-------------------------------------------------------------"
        echo " "
        GITHUB_RESPONSE=$(sudo git clone --depth 1 --branch 10.0 https://www.github.com/odoo/enterprise "/var/lib/odoo/enterprise" 2>&1)
    done

    echo -e "\n---- Added Enterprise code under /var/lib/odoo/enterprise ----"
    echo -e "\n---- Installing Enterprise specific libraries ----"
    sudo pip install sudo
fi

echo -e "\n---- Create custom module directory ----"
sudo su $OE_USER -c "mkdir /var/lib/odoo/custom"

echo -e "\n---- Setting permissions on home folder ----"
sudo chown -R odoo:odoo /var/lib/odoo/*

echo -e "* Change server config file"
sudo sed -i s/"; admin_passwd.*"/"admin_passwd = $OE_SUPERADMIN"/g /etc/odoo/odoo.conf
if [  $IS_ENTERPRISE = "True" ]; then
    sudo su root -c "echo 'addons_path=/var/lib/odoo/custom,/var/lib/odoo/enterprise,/usr/lib/python2.7/site-packages/odoo/addons' >> /etc/odoo/odoo.conf"
else
    sudo su root -c "echo 'addons_path=/var/lib/odoo/custom,/usr/lib/python2.7/site-packages/odoo/addons' >> /etc/odoo/odoo.conf"
fi

echo -e "* Change default xmlrpc port"
sudo su root -c "echo 'xmlrpc_port = $OE_PORT' >> /etc/odoo/odoo.conf"

echo -e "* Start ODOO on Startup"
sudo systemctl enable --now odoo

#Give ownership of odoo file to odoo user
sudo chown -R odoo:odoo /var/lib/odoo


echo -e "* Starting Odoo Service"
echo "-----------------------------------------------------------"
echo "Done! The Odoo server is up and running. Specifications:"
echo "Port: $OE_PORT"
echo "User service: odoo"
echo "User PostgreSQL: odoo"
echo "Code location: /var/lib/odoo"
echo "Addons folder: /var/lib/odoo/custom"
echo "Start Odoo service: sudo systemctl start odoo"
echo "Stop Odoo service: sudo systemctl stop odoo"
echo "Restart Odoo service: sudo systemctl restart odoo"
echo "-----------------------------------------------------------"
